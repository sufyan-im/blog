module.exports = {
  flags: {
    DEV_SSR: false
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-netlify-cms',
      options: {}
    },
    {
      resolve: '@elegantstack/gatsby-theme-flexiblog-science',
      options: {
        // Theme options goes here
        sources: {
          local: true
        }
      }
    }
  ],
  siteMetadata: {
    //General Site Metadata
    title: 'Sufyen',
    name: 'Blog',
    description: "Sufyen's blog",
    address: 'France',
    email: 'email@example.com',
    phone: '+1 (888) 888-8888',

    //Site Social Media Links
    social: [
      {
        name: 'Twitter',
        url: 'https://twitter.com/gatsbyjs'
      },
    ],
    //Header Menu Items
    headerMenu: [
      {
        name: 'Home',
        slug: '/'
      },
      {
        name: 'A propos',
        slug: '/authors'
      },
      {
        name: 'Contact',
        slug: '/contact'
      }
    ],

    //Footer Menu Items (2 Sets)
    footerMenu: [
      {
        title: 'Legal',
        items: [
          {
            name: 'Politique de confidentialité',
            slug: '/'
          },
          {
            name: 'Cookie Policy',
            slug: '/'
          },
          {
            name: 'Terms Of Use',
            slug: '/'
          }
        ]
      }
    ]
  }
}
